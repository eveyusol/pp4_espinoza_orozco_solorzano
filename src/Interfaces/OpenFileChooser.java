/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interfaces;
import Componentes.Album;
import Componentes.Foto;
import Componentes.ImageGallery;
import Componentes.UsersSerializable;
import Componentes.Usuario;
import ManejoFotos.SomeClass;
import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
 

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
 
public class OpenFileChooser {
 
    private Desktop desktop = Desktop.getDesktop();
    private static Stage primaryStage; 
    private  VBox root; 
    //botones principales
    private Button mostrarAlbums; private Button crearAlbum;
    //lablel
    private Label lbldescripcion;
    private Label lblLugar;
    private Label fecha;
    private Label personas;
    //txt
    private TextArea txtdescripcion;
    private TextField txtLugar;
    private TextField txtfecha;
    private TextArea txtPersonas; 
    private ComboBox cbAlbum;
    private Button agregaraAlbum;
    private static List<Album>Album=new ArrayList<>();
    //carga de foto
    TextArea textArea;
    Button button1;
    Button buttonM;
//mostrar albums    
//private ComboBox buscarPor;
    HBox h1;
    HBox h2;
    HBox h3;
    HBox h4;
    HBox h5;
    HBox h6;
    //usuario
    Usuario usuario;

    private List<File> files;
    
    public OpenFileChooser(Stage window, Usuario user) {
        
         
         this.primaryStage=window;
         usuario=user;
 
        this.root = new VBox();
        root.setPadding(new Insets(10));
        root.setSpacing(5);
        iniciarControles();
        cargaFoto(user);
        root.getChildren().addAll(textArea, button1, buttonM,h6);
       
        
    }
    void cargaFoto(Usuario user){
                textArea = new TextArea();
        textArea.setMinHeight(70);
 
        button1 = new Button("Select One File and Open");
 
        buttonM = new Button("Select Multi Files");
 
        button1.setOnAction(new EventHandler<ActionEvent>() {
 
            @Override
            public void handle(ActionEvent event) {
                textArea.clear();
                final FileChooser fileChooser = new FileChooser();
                File file = fileChooser.showOpenDialog(primaryStage);
                if (file != null) {
                    try {
                        openFile(file);
                        files = Arrays.asList(file);
                        printLog(textArea, files);
                        saveImages(user);
                    } catch (IOException ex) {
                        Logger.getLogger(OpenFileChooser.class.getName()).log(Level.SEVERE, null, ex);
                    }
                     
                }
            }
        });
 
        buttonM.setOnAction(new EventHandler<ActionEvent>() {
 
            @Override
            public void handle(ActionEvent event) {
                final FileChooser fileChooser = new FileChooser();
                files = fileChooser.showOpenMultipleDialog(primaryStage);
                printLog(textArea, files);
                try {
                    saveImages(user);
                } catch (IOException ex) {
                    Logger.getLogger(OpenFileChooser.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
        
    }
    
    void iniciarControles(){
        this.h1 =new HBox();
        this.h2 =new HBox();
        this.h3 =new HBox();
        this.h4 =new HBox();
        this.h5 =new HBox();
        this.h6 =new HBox();
        
        this.mostrarAlbums=new Button("Mostrar Albums");
        //this.mostrarAlbums.setOnMouseClicked(e->mostrarAlbum());
        this.crearAlbum=new Button("Crear Album");
        this.crearAlbum.setOnMouseClicked(e->crearPanelAlbum());
        this.lbldescripcion =new Label("Ingrese una descripcion: ");
        this.lblLugar =new Label("Ingrese lugar de la foto: ");
        this.fecha =new Label("Ingrese la fecha: ");
        this.personas =new Label("Ingrese nombre de personas \n"+"separadas por coma: ");

        
        
        this.txtdescripcion =new TextArea();
        this.txtdescripcion.setPromptText("Ingrese descripcion");
        this.txtLugar =new TextField();
        this.txtLugar.setPromptText("Ingrese lugar");
        this.txtfecha =new TextField();
        this.txtfecha.setPromptText("Ingrese fecha");
        this.txtPersonas =new TextArea();
        this.txtPersonas.setPromptText("Ingrese nombre de Personas");
        this.cbAlbum=new ComboBox();
        if(!Album.isEmpty()){
            for(Album a:Album){
                cbAlbum.getItems().add(a.getNombre());
            }  
           agregaraAlbum=new Button("Añadir al album");
           agregaraAlbum.setOnAction(e->{
                try {
                    addAlbum();
                } catch (IOException ex) {
                    Logger.getLogger(OpenFileChooser.class.getName()).log(Level.SEVERE, null, ex);
                }
            });
           h6.getChildren().addAll(cbAlbum,agregaraAlbum);
        }
        
        
        h1.getChildren().addAll(lbldescripcion,txtdescripcion);
        h1.setAlignment(Pos.CENTER);
        h2.getChildren().addAll(lblLugar,txtLugar);
        h2.setMaxSize(600,10);
        h2.setAlignment(Pos.CENTER);
        h3.getChildren().addAll(fecha,txtfecha);
        h3.setAlignment(Pos.CENTER);
        h4.getChildren().addAll(personas,txtPersonas);
        h4.setAlignment(Pos.CENTER);
        
        h5.getChildren().addAll(mostrarAlbums,crearAlbum);
        h5.setAlignment(Pos.CENTER);
        h1.setSpacing(70);
        h2.setSpacing(30);
        h3.setSpacing(30);
        h4.setSpacing(50);
        h5.setSpacing(50);
        
        root.getChildren().addAll(h5,h1,h2,h3,h4);
        root.setSpacing(10);
        root.setAlignment(Pos.BOTTOM_LEFT);
        root.setPrefSize(900,900);
        

    }

    public static List<Album> getAlbum() {
        return Album;
    }
    
    void crearPanelAlbum(){
        this.crearAlbum.setDefaultButton(true);
        PanelAlbum paneAlbum=new PanelAlbum(crearAlbum,root);
        root.getChildren().add(0, paneAlbum.getRoot());
        root.setSpacing(30);
        VentanaAplicacion.getRoot().setCenter(root);        
        
    }
    void addAlbum() throws IOException{
        saveImages(usuario);
    }
    ArrayList<String> listadoPersonas(){
        String[] personas=txtPersonas.getText().split(",");
         ArrayList<String> Listpersonas=new ArrayList<>();
        for(String s:personas){
            Listpersonas.add(s);
        }
        return Listpersonas;
    }

    void saveImages(Usuario user) throws IOException{
        List<byte[]>byteImagenes=SomeClass.Imagenes_Bytes(files);
        Date fecha=new Date(this.fecha.getText());
        Foto f=new Foto(fecha,listadoPersonas(),txtdescripcion.getText(),cbAlbum.getValue().toString(),txtLugar.getText());
        
        ImageGallery iG=new ImageGallery();
        iG.setByteImages(byteImagenes);
        //Album a=new Album();
        VentanaAplicacion.deserializar();
        List<Usuario>usuarios=VentanaAplicacion.getUsers().getListaUsers();
        for(Usuario u:usuarios){
            if(user.equals(u)){
               usuarios.remove(u);
               //u.setGaleria(galeria);
               usuarios.add(u);
                
            }
        }
        VentanaAplicacion.serializar();
        
        SomeClass s=new SomeClass();
        String nomArchivo="foto1"; 
        try {
            s.serializarImagenes_en_Bytes(nomArchivo, files);
            } catch (IOException ex) {
            Logger.getLogger(OpenFileChooser.class.getName()).log(Level.SEVERE, null, ex);
        }
        cargarImagenes(s);
        
    }
    void descargarImagenes(){
        SomeClass s=new SomeClass();
        String nomArchivo="foto1"; 
        s.deserializarBytes(nomArchivo);
        cargarImagenes(s);
    }
    
    
    void cargarImagenes(SomeClass s){
        try {
            List<Image>imagenes=s.getImages();
            for(Image im:imagenes){
                ImageView i=new ImageView();
                i.setImage(im);
                i.setFitWidth(100);
                i.setPreserveRatio(true);
                i.setSmooth(true);
                i.setCache(true);
                root.getChildren().add(i);
            }
            ;
        } catch (IOException ex) {
            Logger.getLogger(OpenFileChooser.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    public VBox getRoot() {
        return root;
    }
    
    private void printLog(TextArea textArea, List<File> files) {
        if (files == null || files.isEmpty()) {
            return;
        }
        for (File file : files) {
            textArea.appendText(file.getAbsolutePath() + "\n");
        }
    }
 
    private void openFile(File file) {
        try {
            this.desktop.open(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public List<File> getFiles() {
        return files;
    }

    public void setFiles(List<File> files) {
        this.files = files;
    }


    
 


}