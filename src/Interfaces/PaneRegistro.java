/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interfaces;

import CambiarDeVentana.CambioVentana;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import static proyectofinalavance.ProyectoFinalAvance.ALTO;
import static proyectofinalavance.ProyectoFinalAvance.ANCHO;

/**
 *
 * @author User
 */
public class PaneRegistro {
    StackPane pane;
    Label nombreUsuario;
    TextField txtnombre;
    TextField txtapellido;
    TextField txtUsuario;
    PasswordField txtContraseña;
    Label lblgender;
    ComboBox gender;
    Button btnRegistrarse;
    Button btnCancelar;
    VBox b;
    HBox h1;
    HBox h2;
    HBox h3;
    HBox h4;
    HBox h5;

    VentanaAplicacion aplicacion;//= new PanelIncio(Window);

    public PaneRegistro(){
        this.pane=new StackPane();
        this.b =new VBox();
        this.h1 =new HBox();
        this.h2 =new HBox();
        this.h3 =new HBox();
        this.h4 =new HBox();
        this.h5 =new HBox();
        
        this.nombreUsuario =new Label("Ingrese su nombre de Usuario: ");
        this.txtnombre =new TextField();
        this.txtnombre.setPromptText("Ingrese su nombre");
        this.txtapellido =new TextField();
        this.txtapellido.setPromptText("Ingrese su apellido");
        this.txtUsuario =new TextField();
        this.txtUsuario.setPromptText("Ingrese nombre de Usuario");
        txtUsuario.setMaxSize(6000, 40);
        txtUsuario.setOnKeyTyped(e->validarTexto(e));
        this.txtContraseña=new PasswordField();
        this.txtContraseña.setPromptText("Ingrese su contraseña");
        this.lblgender=new Label("GENERO");
        this.gender=new ComboBox();
        gender.getItems().addAll("Masculino","Femenino");
        this.btnRegistrarse=new Button("Registrarse ");
        btnRegistrarse.setOnMouseClicked(e->{
            try {
                crearUsuario();
            } catch (IOException ex) {
                Logger.getLogger(PaneRegistro.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
        this.btnCancelar = new Button("Cancelar ");
        btnCancelar.setOnMouseClicked(e->volverPantallaInicio());
        h1.getChildren().addAll(txtnombre,txtapellido);
        h1.setAlignment(Pos.CENTER);
        h2.getChildren().addAll(this.txtUsuario);
        h2.setMaxSize(600,10);
        h2.setAlignment(Pos.CENTER);
        h3.getChildren().addAll(this.txtContraseña);
        h3.setAlignment(Pos.CENTER);
        h4.getChildren().addAll(lblgender,gender);
        h4.setAlignment(Pos.CENTER);
        
        h5.getChildren().addAll(btnRegistrarse,btnCancelar);
        h5.setAlignment(Pos.CENTER);
        h1.setSpacing(10);
        h2.setSpacing(50);
        h3.setSpacing(50);
        h4.setSpacing(50);
        h5.setSpacing(50);
        
        b.getChildren().addAll(h1,h2,h3,h4,h5);
        b.setSpacing(20);
        b.setAlignment(Pos.CENTER);
        pane.getChildren().add(b);
        pane.setPrefSize(700,600);
    }

    void crearUsuario() throws IOException {
        VentanaAplicacion aplicacion=new VentanaAplicacion();
        aplicacion.registrarUsuario(txtUsuario.getText(),txtContraseña.getText(),txtnombre.getText(),txtapellido.getText(),gender.getValue().toString());
        CambioVentana c=new CambioVentana(aplicacion.getRoot());
        //Scene scene= new Scene(juego.getRoot());
        //Window.setScene(scene);
        
    }
    

    public StackPane getRoot() {
        return pane;
    }

   

   

    void validarTexto(KeyEvent event) {
        char car=event.getCharacter().charAt(0);
        if(!(Character.isLetter(car)|| Character.isSpaceChar(car))){
            event.consume();
        }
    }

    void volverPantallaInicio() {
        Sesion paneInicio= new Sesion();
        CambioVentana c=new CambioVentana(paneInicio.getRoot(),ALTO, ANCHO);

        // Scene scene2= new Scene(paneInicio.getRoot());
        //Window.setScene(scene2);

    }

}
