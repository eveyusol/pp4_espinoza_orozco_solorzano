/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interfaces;

import Componentes.Album;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;

/**
 *
 * @author jdavi
 */
public class PanelAlbum {
    
    private Pane paneAlbum=new Pane();
    private Label lblnombre;
    private Label lbldescripcion;
    private TextArea txtdescripcion;
    private TextField txtnombre;
    private String rutaIconosubirFoto="/Imagenes/subiFotoIcono.png";
    private ImageView i;
    private VBox b1;
    private VBox b2;
    private HBox h1;
    private HBox h2;   
    private HBox h3;
    private Button aceptar;
    public PanelAlbum(Button crearAlbum, VBox raiz) {
        this.lblnombre=new Label("nombre del Album: ");
        this.lbldescripcion=new Label("Ingrese una descripcion: ");
        this.txtnombre =new TextField();
        this.txtnombre.setPromptText("Ingrese nombre del Album");
        this.txtdescripcion =new TextArea();
        this.txtdescripcion.setPromptText("Ingrese descripcion");
        
        //i=new ImageView(new Image(this.rutaIconosubirFoto));
        this.b1=new VBox();
        this.b2=new VBox();
        this.h1=new HBox();
        h1.getChildren().addAll(lblnombre,txtnombre);
        this.h2=new HBox();
        h1.getChildren().addAll(lbldescripcion,txtdescripcion);
        this.aceptar=new Button("ACEPTAR");
        aceptar.setOnAction(e->click(crearAlbum,raiz));
        this.b1.getChildren().addAll(h1,h2);
        this.h3=new HBox();
        this.h3.getChildren().addAll(b1,aceptar);
        this.paneAlbum.getChildren().addAll(h3);
        
    }

    public Pane getRoot() {
        return paneAlbum;
    }


    private void click(Button crearAlbum, VBox raiz) {
        Album a=new Album(txtnombre.getText(),txtdescripcion.getText());
        OpenFileChooser.getAlbum().add(a);
        raiz.getChildren().remove(0);
        VentanaAplicacion.getRoot().setCenter(raiz);
        crearAlbum.setDefaultButton(false);
        

        
    }
    
        
        
    
        
         
}
