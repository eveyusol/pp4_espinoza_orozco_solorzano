/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interfaces;

import Componentes.Usuario;
import Componentes.UsersSerializable;
import CambiarDeVentana.CambioVentana;
import static Globales.TamanoPantalla.ALTO;
import static Globales.TamanoPantalla.ANCHO;
import java.awt.Color;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Paint;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javax.swing.JOptionPane;
import proyectofinalavance.ProyectoFinalAvance;

/**
 *
 * @author User
 */
public class Sesion {
    //
    public final static double ALTO= 700;
    public final static int ANCHO= 780;
    //
    private Button login;
    private Button cancelar;
    private Label lblUsuario;
    private TextField txtUsuario;
    private Label lblcontrasenia;
    private PasswordField passwordField;
    private HBox hbox;
    private HBox hbox1;
    private VBox vbox;
    private HBox hbox2;
    private VBox vboxRegistro;
    private Label LabelRegistro;
    private StackPane contenedor;
    private ImageView imagenFondo;

    private static UsersSerializable users;

    public Sesion() {
        inicializarControles();
        this.login.setOnMouseClicked((event)->inicioSesion());
        this.cancelar.setOnMouseClicked((event)->cancelarSesion());
        this.LabelRegistro.setOnMouseClicked((event)->Registro());
       
        
    }
    private void inicializarControles(){
        //iniciando las etiquetas y botones
        this.login = new Button("LOGIN");
        this.cancelar =new Button("CANCEL");
        this.lblUsuario =new Label("USER:     ");
        lblUsuario.setFont(Font.font("Arial",FontWeight.EXTRA_BOLD,25));
        lblUsuario.setStyle("-fx-text-fill: white;");
        this.txtUsuario =new TextField();
        this.txtUsuario.setPromptText("Ingrese su Usuario");
        this.lblcontrasenia =new Label("Password: ");
        lblcontrasenia.setFont(Font.font("Arial",FontWeight.EXTRA_BOLD,25));
        lblcontrasenia.setStyle("-fx-text-fill: white;");
        this.passwordField = new PasswordField();
        passwordField.setPromptText("Your password");
        //Contenedor HBox qu contiene label y su respectivo textfield
        this.hbox =new HBox();
        hbox.setSpacing(20);
        hbox.setAlignment(Pos.CENTER);
        hbox.getChildren().addAll(lblUsuario,txtUsuario);
        //Contenedor HBox qu contiene label y su respectivo textfield
        this.hbox1 =new HBox();
        hbox.setSpacing(20);
        hbox1.setAlignment(Pos.CENTER);
        hbox1.getChildren().addAll(lblcontrasenia,passwordField);
        //Contenedor HBox qu contiene botones de login y cancel
        this.hbox2 =new HBox();
        hbox2.setSpacing(20);
        hbox2.setAlignment(Pos.CENTER);
        hbox2.getChildren().addAll(login,cancelar);
        //Contenedor VBox que contiene label de registrarse
        this.vboxRegistro=new VBox();
        this.LabelRegistro = new Label("Registrarse ahora");
        this.LabelRegistro.setTextFill(javafx.scene.paint.Color.web("#0000FF"));
        this.LabelRegistro.setStyle("-fx-font-weight: bold");
        vboxRegistro.getChildren().add(LabelRegistro);
        vboxRegistro.setSpacing(20);
        vboxRegistro.setAlignment(Pos.CENTER);
        
        this.vbox=new VBox();
        vbox.setSpacing(20);
        vbox.setAlignment(Pos.CENTER);
        vbox.getChildren().addAll(hbox,hbox1,vboxRegistro,hbox2);
        contenedor=new StackPane();
        contenedor.getChildren().addAll(vbox);
        
        contenedor.setStyle("-fx-background-image: url('/Imagenes/fondo.PNG'); "
            + "-fx-background-position: center center; "
            + "-fx-background-repeat: stretch;"
            + "-fx-background-size:"+ ALTO +" "+ ANCHO + ";"
            );contenedor.alignmentProperty();
        contenedor.setPrefSize(1500,2000);
        contenedor.autosize();
        
        
    }

    public StackPane getRoot() {
        return contenedor;
    }

    public void setRoot(StackPane contenedor) {
        this.contenedor = contenedor;
    }
    

    void inicioSesion() {
        VentanaAplicacion.deserializar();
        users=VentanaAplicacion.getUsers();
        validarUsuario(txtUsuario.getText(),passwordField.getText());
        

    }

    void cancelarSesion() {
        
    }

    void Registro() {
        PaneRegistro r=new PaneRegistro();
        CambioVentana c=new CambioVentana(r.getRoot());
    }
    
    public  void validarUsuario(String userName, String password) {
        boolean userCorrecto=false;
        boolean nomUser=false;
        if(users!=null){
            for(Usuario u:users.getListaUsers()){
                if(u.getUserName().equals(userName)){
                    nomUser=true;
                    if(u.getUserName().equals(userName)){
                        userCorrecto=true;
                        VentanaAplicacion aplicacion=new VentanaAplicacion();
                        aplicacion.setUser(u);
                        double dimAlto=1000;
                        double dimbajo=1000;
                        CambioVentana c=new CambioVentana(aplicacion.getRoot(),dimAlto,dimbajo);
                    }
                }
            }
            if(userCorrecto==false){
                Sesion s=new Sesion();
                CambioVentana c=new CambioVentana(s.getRoot(),ALTO, ANCHO);
                if(nomUser){
                  JOptionPane.showMessageDialog(null, "Contraseña de Usuario es incorrecta", "Alerta", JOptionPane.WARNING_MESSAGE);  
                }
                JOptionPane.showMessageDialog(null, "Usuario y/o contraseña incorrecta o no esta registrado ", "Alerta", JOptionPane.WARNING_MESSAGE);
            }
        }
        
    }

    
    
}
