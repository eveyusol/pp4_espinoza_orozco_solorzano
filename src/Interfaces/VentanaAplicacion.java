/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interfaces;

import Interfaces.OpenFileChooser;
import CambiarDeVentana.CambioVentana;
import Componentes.UsersSerializable;
import Componentes.Usuario;
import Globales.TamanoPantalla;
import static Globales.TamanoPantalla.ALTO;
import static Globales.TamanoPantalla.ANCHO;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javax.swing.JOptionPane;
import proyectofinalavance.ProyectoFinalAvance;


/**
 *
 * @author User
 */
public class VentanaAplicacion {
    private static BorderPane root;
    //panel superior del border pane
    private StackPane paneSuperior;
    //menu
    private MenuBar menuBar;
    private Menu menu;
    private MenuItem item1;
    private TextField buscador;
    private Button agregaraAlbum;
    private ImageView lupa;
    private HBox h;
    private HBox h1;
    private static UsersSerializable users;
    private Usuario user;
    private VBox b;
    

    public VentanaAplicacion() {
        iniciarControles();
        users=new UsersSerializable();
        
    }
    private void iniciarControles(){
        this.b=new OpenFileChooser(CambioVentana.getWindow(),user).getRoot();
        
        this.root=new BorderPane();
        root.setPrefSize(TamanoPantalla.ANCHO,TamanoPantalla.ALTO);
        this.menuBar=new MenuBar();
        this.menu = new Menu("MENU");
        item1=new MenuItem("SALIR");
        item1.setOnAction(e->System.exit(0));
        this.menu.getItems().addAll(item1);
        this.menuBar.getMenus().addAll(menu);
        this.buscador=new TextField();
        buscador.setPromptText("Buscador");
        
        this.lupa=new ImageView(new Image("/Imagenes/lupa.png"));
        this.lupa.setFitHeight(30);
        this.lupa.setFitWidth(30);
        this.lupa.setOnMouseClicked((event)->clicBuscar());
        this.h=new HBox();
        h.getChildren().addAll(buscador,lupa);
        h.setSpacing(20);
        h.setAlignment(Pos.CENTER);
        this.h1=new HBox();
        h1.getChildren().addAll(h,menuBar);
        h1.setSpacing(250);
        h1.setAlignment(Pos.CENTER);
        h1.setMinSize(200,80);
        paneSuperior=new StackPane();
        paneSuperior.getChildren().addAll(h1,b);
        root.setTop(paneSuperior);
        paneSuperior.setAlignment(Pos.CENTER);
        paneSuperior.setStyle("-fx-background-color: blue;");
        root.setCenter(b);
        
        
    }
    

    

    static BorderPane getRoot() {
        return root;
    }

    void registrarUsuario(String userName, String password, String nombres, String apellidos, String gender) throws IOException {
        Usuario u=new Usuario(userName,password,nombres,apellidos,gender);
        agregarUsuario(u);
        
       
    }

    void agregarUsuario(Usuario u) throws IOException {
        File file = new File("UsuariosSerializados.txt"); 
        boolean empty =file.length() == 0; 
        if(file.exists()){
            if(!empty){
                deserializar(); 
                users.add(u);
            }else{
               users.add(u);
               serializar();
            }
        }else{
            JOptionPane.showMessageDialog(null, "El archivo con los usuarios no existe ", "Alerta Roja", JOptionPane.WARNING_MESSAGE);
        }
        
        
        
    }

    public void setUser(Usuario user) {
        this.user = user;
    }

    public static void setUsers(UsersSerializable users) {
        VentanaAplicacion.users = users;
    }

    public static UsersSerializable getUsers() {
        return users;
    }
    
    
     private void clicBuscar() {
        
    }
    public static void  serializar() throws IOException{
        FileOutputStream f= null;
                    ObjectOutputStream s=null;
        try {
             f= new FileOutputStream("UsuariosSerializados.txt");
            s = new ObjectOutputStream(f);
            s.writeObject(users);
            
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ProyectoFinalAvance.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ProyectoFinalAvance.class.getName()).log(Level.SEVERE, null, ex);
        }
        finally{
            f.close();
            s.close();
        }
    }
    public static void deserializar(){
        try(ObjectInputStream o = new ObjectInputStream( new FileInputStream("UsuariosSerializados.txt"))){
            users= (UsersSerializable)o.readObject();
            System.out.println("Deserializado"+users);
            
        }
       catch (FileNotFoundException ex) {
            Logger.getLogger(ProyectoFinalAvance.class.getName()).log(Level.SEVERE, null, ex);
        }catch (IOException | ClassNotFoundException ex) {
            Logger.getLogger(ProyectoFinalAvance.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

   

 
    
}
