/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ManejoFotos;


 
import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

 
public class OpenFileChooserDemo extends Application {
 
    private Desktop desktop = Desktop.getDesktop();
    VBox root;
    
    @Override
    public void start(Stage primaryStage) throws Exception {
 
        final FileChooser fileChooser = new FileChooser();
 
        TextArea textArea = new TextArea();
        textArea.setMinHeight(70);
 
        Button button1 = new Button("Select One File and Open");
 
        Button buttonM = new Button("Select Multi Files");
 
        button1.setOnAction(new EventHandler<ActionEvent>() {
 
            @Override
            public void handle(ActionEvent event) {
                textArea.clear();
                File file = fileChooser.showOpenDialog(primaryStage);
                if (file != null) {
                    openFile(file);
                    List<File> files = Arrays.asList(file);
                    System.out.println(file);
                    printLog(textArea, files);
                    SomeClass s=new SomeClass();
                    String nomArchivo="foto1"; 
                    /* try {
                    s.serializarImagenes_en_Bytes(nomArchivo, files);
                    } catch (IOException ex) {
                    Logger.getLogger(OpenFileChooserDemo.class.getName()).log(Level.SEVERE, null, ex);
                    }*/
                    s.deserializarBytes(nomArchivo);
                    /*SomeClass s=new SomeClass();
                        String nomArchivo="foto1";
                        Image image = null;
                        try {
                        image = new Image(files.get(0).toURI().toURL().toExternalForm(), false);
                        } catch (MalformedURLException ex) {
                        Logger.getLogger(OpenFileChooserDemo.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        s.setImage(image);
                        try {
                        s.writeObject(nomArchivo);
                        } catch (IOException ex) {
                        Logger.getLogger(OpenFileChooserDemo.class.getName()).log(Level.SEVERE, null, ex);
                        }*/
                    try {
                        List<Image>imagenes=s.getImages();
                        for(Image im:imagenes){
                            ImageView i=new ImageView(im);
                            root.getChildren().add(i);
                        }
                        ;
                    } catch (IOException ex) {
                        Logger.getLogger(OpenFileChooserDemo.class.getName()).log(Level.SEVERE, null, ex);
                    }
                        
                }/*SomeClass s=new SomeClass();
                String nomArchivo="foto1";
                try {
                    s.readObject(nomArchivo);
                } catch (IOException ex) {
                    Logger.getLogger(OpenFileChooserDemo.class.getName()).log(Level.SEVERE, null, ex);
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(OpenFileChooserDemo.class.getName()).log(Level.SEVERE, null, ex);
                }*/
                
                
            }
        });
 
        buttonM.setOnAction(new EventHandler<ActionEvent>() {
 
            @Override
            public void handle(ActionEvent event) {
                textArea.clear();
                List<File> files = fileChooser.showOpenMultipleDialog(primaryStage);
                System.out.println(files);
                printLog(textArea, files);
                SomeClass s=new SomeClass();
                String nomArchivo="foto2"; 
                try {
                s.serializarImagenes_en_Bytes(nomArchivo, files);
                } catch (IOException ex) {
                Logger.getLogger(OpenFileChooserDemo.class.getName()).log(Level.SEVERE, null, ex);
                }
                //s.deserializarBytes(nomArchivo);
                try {
                        List<Image>imagenes=s.getImages();
                        for(Image im:imagenes){
                            ImageView i=new ImageView(im);
                            root.getChildren().add(i);
                        };
                } catch (IOException ex) {
                    Logger.getLogger(OpenFileChooserDemo.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
 
         root = new VBox();
        root.setPadding(new Insets(10));
        root.setSpacing(5);
        root.getChildren().addAll(textArea, button1, buttonM);
 
        Scene scene = new Scene(root, 400, 200);
 
        primaryStage.setTitle("JavaFX FileChooser");
        primaryStage.setScene(scene);
        primaryStage.show();
    }
 
    private void printLog(TextArea textArea, List<File> files) {
        if (files == null || files.isEmpty()) {
            return;
        }
        for (File file : files) {
            textArea.appendText(file.getAbsolutePath() + "\n");
        }
    }
 
    private void openFile(File file) {
        try {
            this.desktop.open(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
 
    public static void main(String[] args) {
        launch(args);
    }
 


}