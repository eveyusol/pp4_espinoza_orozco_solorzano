/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ManejoFotos;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.image.Image;
import javax.imageio.ImageIO;
import org.apache.pdfbox.jbig2.Bitmap;


/**
 *
 * @author jdavi
 */
public class SomeClass implements Serializable {
    //private transient Image image ;
    private List<Image> images=new ArrayList<>() ;
    //private transient Image image;
    private List<byte[]> byteImages;
    // other fields, etc...
    public SomeClass() {
        
    }
    
    public void serializarImagenes_en_Bytes(String nomArchivo,List<File>files) throws IOException{
        creaFile(nomArchivo);
        FileOutputStream f= null;
                    ObjectOutputStream s=null;
        try {
             f= new FileOutputStream(nomArchivo);
            s = new ObjectOutputStream(f);          
            byteImages=Imagenes_Bytes(files);
            s.writeObject(byteImages);
            
            
        } catch (FileNotFoundException ex) {
            Logger.getLogger(SomeClass.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(SomeClass.class.getName()).log(Level.SEVERE, null, ex);
        }
        finally{
            f.close();
            s.close();
        }
        
    }
    public static List<byte[]> Imagenes_Bytes(List<File>files) throws IOException{
        List<byte[]>byteImagenes=new ArrayList<>();
        for(File f:files){
            byte[] fileContent = Files.readAllBytes(f.toPath());
            byteImagenes.add(fileContent);
        }
        return byteImagenes;
    }
    
    public void deserializarBytes(String nomArchivo){
        try(ObjectInputStream o = new ObjectInputStream( new FileInputStream(nomArchivo))){
            byteImages= (List<byte[]>)o.readObject();
            System.out.println("Deserializado");
        }
       catch (FileNotFoundException ex) {
            Logger.getLogger(SomeClass.class.getName()).log(Level.SEVERE, null, ex);
        }catch (IOException | ClassNotFoundException ex) {
            Logger.getLogger(SomeClass.class.getName()).log(Level.SEVERE, null, ex);
        }
       
    }
    
    public static List<Image> Bytes_Imagenes(List<byte[]>byteImagenes) throws IOException{
        List<Image>Imagenes=new ArrayList<>();
        for(byte[]b:byteImagenes){
            Image img = SwingFXUtils.toFXImage(ImageIO.read(new ByteArrayInputStream(b)), null);
            Imagenes.add(img);
        }
        return Imagenes;
    }
  
    public void creaFile(String nomArchivo){
         File fichero=new File(nomArchivo);
        if(!fichero.exists()){
            try{
                fichero.createNewFile();
                System.out.println(fichero.getName()+" ha sido creado");
            }catch (IOException ex){ex.printStackTrace();}
            
        }else{
            System.out.println("El archivo ya existe");
        }
    }

    public List<Image> getImages() throws IOException {
        images=Bytes_Imagenes(byteImages);
        return images;
    }

    public void setImages(Image image) {
        this.images.add(image);
    }
       
}