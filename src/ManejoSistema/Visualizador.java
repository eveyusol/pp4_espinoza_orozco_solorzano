/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ManejoSistema;

import Componentes.Album;
import Componentes.Foto;
import java.util.ArrayList;

/**
 *
 * @author josè
 */
public class Visualizador {
    
    private ArrayList<Album> galeria;

    public Visualizador(ArrayList<Album> galeria) {
        this.galeria = galeria;
    }
    
    public ArrayList<Foto> porFecha(int fechaInicioMes, int fechaInicioAño, int fechaFinalMes,int fechaFinalAño ){
        
        ArrayList<Foto> resultadoVisualizar = new ArrayList();
        
        for (Album album: this.galeria) {
            for (Foto foto: album.getFotos()) {
                /*String[] fecha = foto.getFecha().split("/");
               
                for (int i = fechaInicioAño; i <= fechaFinalAño; i++) {
                    
                    if (i == Integer.parseInt(fecha[2]) ){
                        
                        for (int j = fechaInicioMes; j <= fechaFinalMes; i++) {
                            if (j == Integer.parseInt(fecha[1]) ){
                                resultadoVisualizar.add(foto);
                            }
                        }
                    }
                }
            
          */  }
        }
        
        return resultadoVisualizar;   
    }
    //metodo que busca las fotos de la galeria por una rango de fecha y retrona una lista con todas las fotos encontradas
    
    public ArrayList<Foto> porLugares(String lugar){
        
        ArrayList<Foto> resultadoVisualizar = new ArrayList();
        
        for (Album album: this.galeria) {
            for (Foto foto: album.getFotos()) {
                if (lugar.equals(foto.getLugar())){
                    resultadoVisualizar.add(foto);
                }
            }
        }
        
        return resultadoVisualizar;
        
    }
    //Metodo que busca foto con un lugar en especifico y luego crea una lista con esas fotos
    
    public ArrayList<Foto> porPersona(String persona){
        
        ArrayList<Foto> resultadoVisualizar = new ArrayList();
        
        for (Album album: this.galeria) {
            for (Foto foto: album.getFotos()) {
                for (String etiquetados: foto.getPersonas()) {
                    
                    if (persona.equals(etiquetados)){
                        resultadoVisualizar.add(foto);
                    }
                
                }
            }
        }
        return resultadoVisualizar;
    }
    //Metodo que busaca el nombre de una persona en alguna foto y luego la foto que lo contenga es guardada en una lista
}
