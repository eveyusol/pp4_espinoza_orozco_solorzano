/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ManejoSistema;


import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;


public class ManejoArchivos {
  
    /*Usando NIO*/
    public static List<String> lecturaArchivoSimple(String inFilename) throws IOException{
        Charset charset = Charset.forName("ISO-8859-1");
        Path path = Paths.get(inFilename);
        return Files.readAllLines(path);
        
    }
    /*Usando NIO*/
    public static void escrituraArchivosSimple(List<String> escritura, String outFileName) throws IOException{
       Files.write(Paths.get(outFileName), escritura);
      
    }
}
