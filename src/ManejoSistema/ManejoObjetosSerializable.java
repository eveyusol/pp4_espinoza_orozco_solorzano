/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ManejoSistema;

import Componentes.UsersSerializable;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import proyectofinalavance.ProyectoFinalAvance;

/**
 *
 * @author User
 */
public class ManejoObjetosSerializable<T>{
    
    private T objeto;

    public ManejoObjetosSerializable(T object) {
        this.objeto=object;
    }
    
    public void  serializar(String ruta) throws IOException{
        FileOutputStream f= null;
                    ObjectOutputStream s=null;
        try {
             f= new FileOutputStream(ruta);
            s = new ObjectOutputStream(f);
            s.writeObject(objeto);
            
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ProyectoFinalAvance.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ProyectoFinalAvance.class.getName()).log(Level.SEVERE, null, ex);
        }
        finally{
            f.close();
            s.close();
        }
    }
    public void deserializar(String ruta){
        //"UsuariosSerializados.txt"
        try(ObjectInputStream o = new ObjectInputStream( new FileInputStream(ruta))){
            objeto= (T)o.readObject();
            System.out.println("Deserializado"+objeto);
        }
       catch (FileNotFoundException ex) {
            Logger.getLogger(ProyectoFinalAvance.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ProyectoFinalAvance.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ProyectoFinalAvance.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public T getObjeto() {
        return objeto;
    }

    public void setObjeto(T objeto) {
        this.objeto = objeto;
    }
    
}
