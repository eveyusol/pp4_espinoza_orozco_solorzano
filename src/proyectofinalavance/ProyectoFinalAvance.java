 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectofinalavance;

import CambiarDeVentana.CambioVentana;
import Interfaces.Sesion;
import java.awt.Image;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.scene.Scene;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;

/**
 *
 * @author josè
 */
public class ProyectoFinalAvance  extends Application {

    /**
     * @param args the command line arguments
     */
    public final static double ALTO= 700;
    public final static int ANCHO= 780;
    private ImageView ivImagen;
    private static Image imagen;

     @Override
    public void start(Stage primaryStage) {
        CambioVentana c=new CambioVentana(primaryStage);
        //CambioVentana es una clase que contiene el Stage como un static
        //Ademas contiene una escena y metodos(Contructor) que seteen la escena del stage
       Sesion s= new Sesion();
       Scene scene = new Scene(s.getRoot(), ALTO, ANCHO);

       primaryStage.setTitle("BOOKPHOTOS");
       primaryStage.setScene(scene);
       primaryStage.show();
       
    }  
    public static void main(String[] args) {
       //System.out.println("User"+"\u001B[44m");
       launch(args);
      //AbrirArchivo a=new AbrirArchivo();

   }

    
    
}
