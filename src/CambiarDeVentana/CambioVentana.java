/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CambiarDeVentana;

import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 *
 * @author user
 * @param <T>
 */
public class CambioVentana<T> {
    private static Stage Window;
    private T data;
    private  static Scene scene;

    public CambioVentana() {
        this.Window =null;
        this.data =null;
        this.scene=null;
    }
     public CambioVentana(Stage cambiarWindow) {
        this.Window=cambiarWindow;
        
    }
//cambiar contenedor
    public CambioVentana(T data) {
        this.data = data;
        this.scene= new Scene((Parent) this.data);
        this.Window.setScene(this.scene);
    }
    
      
    public CambioVentana(T dataRoot,double ALTO, double ANCHO) {
        this.data = dataRoot;
        Scene scene2 = new Scene((Parent) this.data,ALTO, ANCHO);
        Window.setScene(scene2);
    }

    public CambioVentana(Stage cambiarWindow, T dataRoot) {
        this.Window = cambiarWindow;
        this.data = dataRoot;
        this.scene= new Scene((Parent) this.data);
        this.Window.setScene(this.scene);
        
    }
  
   

    /**
     *
     * @param dataRoot 
     */
    public void SaltoVentanaWindow(T dataRoot) {
        this.data = dataRoot;
        Scene scene2 = new Scene((Parent) this.data);
        Window.setScene(scene2);
    }

    
    public void SaltoVentanaWindow(T dataRoot,double ALTO, double ANCHO) {
        this.data = dataRoot;
        Scene scene2 = new Scene((Parent) this.data,ALTO, ANCHO);
        Window.setScene(scene2);
    }
    
    public void setWindow(Stage Window) {
        this.Window = Window;
    }

    public static Stage getWindow() {
        return Window;
    }

    public void setDataRoot(T data) {
        this.data = data;
    }

    public static Scene getScene() {
        return scene;
    }

    public static void setScene(Scene scene) {
        CambioVentana.scene = scene;
        
    }

    
    
    
    
    
}
