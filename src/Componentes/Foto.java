/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Componentes;

import java.io.BufferedInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.Objects;

/**
 *
 * @author josè
 */
public class Foto implements Serializable {
    
    private Date fecha;
    private ArrayList<String> personas;
    private String descripcion;
    private String nombreAlbum;
    private String camaraMarca;
    private String camaraModelo;
    private String reaccion;
    private String comentario;
    private String hashtag;
    private String lugar;

    public Foto(Date fecha, ArrayList<String> personas, String descripcion, String nombreAlbum, String lugar) {
        this.fecha = fecha;
        this.personas = personas;
        this.descripcion = descripcion;
        this.nombreAlbum = nombreAlbum;
        this.lugar = lugar;
    }

    
    
    public Foto(Date fecha, ArrayList<String> personas, String descripcion, String nombreAlbum, String camaraMarca, String camaraModelo, String reaccion, String comentario, String hashtag,String lugar) {
        this.fecha = fecha;
        this.personas = personas;
        this.descripcion = descripcion;
        this.nombreAlbum = nombreAlbum;
        this.camaraMarca = camaraMarca;
        this.camaraModelo = camaraModelo;
        this.reaccion = reaccion;
        this.comentario = comentario;
        this.hashtag = hashtag;
        this.lugar = lugar;
    }

    public ArrayList<String> getPersonas() {
        return personas;
    }

    public String getNombreAlbum() {
        return nombreAlbum;
    }

    public Date getFecha() {
        return fecha;
    }

    public String getCamaraMarca() {
        return camaraMarca;
    }

    public String getCamaraModelo() {
        return camaraModelo;
    }

    public String getReaccion() {
        return reaccion;
    }

    public String getHashtag() {
        return hashtag;
    }

    public String getLugar() {
        return lugar;
    }
    

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public void setPersonas(ArrayList<String> personas) {
        this.personas = personas;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public void setNombreAlbum(String nombreAlbum) {
        this.nombreAlbum = nombreAlbum;
    }

    public void setCamaraMarca(String camaraMarca) {
        this.camaraMarca = camaraMarca;
    }

    public void setCamaraModelo(String camaraModelo) {
        this.camaraModelo = camaraModelo;
    }

    public void setReaccion(String reaccion) {
        this.reaccion = reaccion;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    public void setHashtag(String hashtag) {
        this.hashtag = hashtag;
    }

    public void setLugar(String lugar) {
        this.lugar = lugar;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 83 * hash + Objects.hashCode(this.fecha);
        hash = 83 * hash + Objects.hashCode(this.nombreAlbum);
        hash = 83 * hash + Objects.hashCode(this.camaraMarca);
        hash = 83 * hash + Objects.hashCode(this.camaraModelo);
        hash = 83 * hash + Objects.hashCode(this.reaccion);
        hash = 83 * hash + Objects.hashCode(this.lugar);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Foto other = (Foto) obj;
        if (!Objects.equals(this.nombreAlbum, other.nombreAlbum)) {
            return false;
        }
        if (!Objects.equals(this.camaraMarca, other.camaraMarca)) {
            return false;
        }
        if (!Objects.equals(this.camaraModelo, other.camaraModelo)) {
            return false;
        }
        if (!Objects.equals(this.reaccion, other.reaccion)) {
            return false;
        }
        if (!Objects.equals(this.lugar, other.lugar)) {
            return false;
        }
        if (!Objects.equals(this.fecha, other.fecha)) {
            return false;
        }
        if (!Objects.equals(this.personas, other.personas)) {
            return false;
        }
        return true;
    }
    
    


    public static ArrayList<Foto> loadPhotos() {
        ArrayList<Foto> fotos = new ArrayList<>();
        //fotos.add();

        return fotos;
    }

    
    
}
