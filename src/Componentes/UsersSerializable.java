/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Componentes;

import java.io.IOException;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author User
 */
public class UsersSerializable implements Serializable {
    public List<Usuario> listaUsers = new ArrayList <>(); 

    public UsersSerializable() {
    }

    public  List<Usuario> getListaUsers() {
        return listaUsers;
    }

    public void setListaUsers(List<Usuario> listaUsers) {
        this.listaUsers = listaUsers;
    }
    public void add(Usuario u){
        listaUsers.add(u);
    }
    
    
}
