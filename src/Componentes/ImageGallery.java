package Componentes;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.TilePane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;

public class ImageGallery  {

    
    VBox mainContainer;
    private Button allButton;
    private Button uniqueButton;
    private Label totalLabel;
    private HBox toolbar;
    private ScrollPane scrollPane;
    private TilePane gallery;
    private TextField textf;
    private Label buscar;
    private List<byte[]> byteImages;
    private final ArrayList<Foto> fotos = Foto.loadPhotos();
    //private final ArrayList<Album> albums;
    public ImageGallery() {
        mainContainer = new VBox(10);
        mainContainer.setAlignment(Pos.CENTER);

        toolbar = createToolBar();
        mainContainer.getChildren().add(toolbar);
        
        totalLabel = createTotalLabel();
        mainContainer.getChildren().add(totalLabel);
        
        gallery = createTilePane();
        
        scrollPane = createScrollPane();
        displayFotosIn(fotos, gallery);
        scrollPane.setContent(gallery);
        mainContainer.getChildren().add(scrollPane);
        
        uniqueButton.setOnMouseClicked((event)-> {
            //nueva galeria
            TilePane gallery2 = createTilePane();
            //se llena nueva galeria con libros no repetidos
            displayFotosIn(eliminarRepetidos (fotos), gallery2);
            //se cambia el scrollPane con la nueva galeria sin libros repetidos
            displayBooksNoRepetidos(scrollPane, gallery2);
            
        
        });
        
        allButton.setOnMouseClicked((event)->{
            //llenar galeria
             TilePane gallery1 = createTilePane();
            displayFotosIn(fotos, gallery1);
            scrollPane.setContent(gallery1);
            
        });
        
    }

    public void setByteImages(List<byte[]> byteImages) {
        this.byteImages = byteImages;
    }
    
    public VBox getMainContainer() {
        return mainContainer;
    }
    
    
    
   public ArrayList<Foto>  eliminarRepetidos (ArrayList<Foto> fotos){
       Set<Foto> hashSet = new HashSet<Foto>(fotos);
        ArrayList<Foto> fotosNoRepetidos=new ArrayList<>();
        fotosNoRepetidos.addAll(hashSet);
        return fotosNoRepetidos;
   }
   


    private HBox createToolBar() {
        HBox tb = new HBox(20);
        tb.setPadding(new Insets(10, 10, 10, 10));
        tb.setAlignment(Pos.CENTER);
        allButton = createAllButton();
        uniqueButton = createUniqueButton();
        tb.getChildren().add(allButton);
        tb.getChildren().add(uniqueButton);
        return tb;
    }

    private Button createAllButton() {
        Button button = new Button("Show all albums");
        return button;
    }

    private Button createUniqueButton() {
        Button button = new Button("Hide repeated");
        return button;
    }

    private Label createTotalLabel() {
        Label label = new Label("Showing " + fotos.size() + " fotos");
        label.setTextFill(Color.web("#872323"));
        label.setFont(Font.font("Cambria", 20));
        label.setStyle("-fx-font-weight: bold");
        return label;
    }

    private ScrollPane createScrollPane() {
        ScrollPane sp = new ScrollPane();
        sp.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER); // Horizontal
        sp.setVbarPolicy(ScrollPane.ScrollBarPolicy.AS_NEEDED); // Vertical scroll bar
        sp.setFitToWidth(true);
        return sp;
    }

    private TilePane createTilePane() {
        TilePane tilePane = new TilePane();
        tilePane.setAlignment(Pos.CENTER);
        tilePane.setPadding(new Insets(15, 15, 15, 15));
        tilePane.setVgap(30);
        tilePane.setHgap(20);
        return tilePane;
    }

    private void displayFotosIn(ArrayList<Foto> fotos, Pane container) {
        for (Foto foto : fotos) {
            Pane fotoView = createFotoView(foto);
            container.getChildren().addAll(fotoView);
        }
    }
    
    private void displayBooksNoRepetidos(ScrollPane s,Pane container) {
            s.setContent(container);
            
        
    }
    public ArrayList<Foto>  buscarPorYear (ArrayList<Foto> fotos,int year){
       Set<Foto> hashSet = new HashSet<Foto>(fotos);
        ArrayList<Foto> booksPorYear=new ArrayList<>();
        
        Iterator<Foto> iterator = hashSet.iterator();
        while(iterator.hasNext()) {
            Foto b= iterator.next();
            if(b.getFecha().equals(year)) {
                iterator.remove();
            }
        }
        booksPorYear.addAll(hashSet);
        return booksPorYear;
    }

    private Pane createFotoView(Foto foto) {
        System.out.println("Adding: " + foto.getFecha().toString());

        VBox vbox = new VBox();
        //Image image = new Image();
        ImageView imageView = new ImageView();
        imageView.setFitWidth(150);

        vbox.getChildren().add(imageView);

        Label titleLabel = new Label(foto.getLugar());
        titleLabel.setMaxWidth(150);
        vbox.getChildren().add(titleLabel);

        Label yearLabel = new Label("" + foto.getFecha().toString());
        
        yearLabel.setTextFill(Color.web("#0000FF"));
        yearLabel.setStyle("-fx-font-weight: bold");
        yearLabel.setOnMouseClicked((event)->{
           TilePane gallery3 = createTilePane();
            //se llena nueva galeria con libros no repetidos
            displayFotosIn(buscarPorYear(fotos,Integer.parseInt(yearLabel.getText())), gallery3);
            //se cambia el scrollPane con la nueva galeria sin libros repetidos
            displayBooksNoRepetidos(scrollPane, gallery3);
            scrollPane.setContent(gallery3);
             
        });
        vbox.getChildren().add(yearLabel);
 
        
        
        

        return vbox;
    }



}
