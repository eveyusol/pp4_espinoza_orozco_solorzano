/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Componentes;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author josè
 */
public class Usuario implements Serializable {
    
    private String userName;
    private String password;
    private ArrayList<Album> galeria;
    private String nombres;
    private String apellidos;
    private String gender;
    private List<byte[]> byteImages;

    public Usuario() {
        byteImages=new ArrayList<>();
    }

    
    public Usuario(String userName, String password) {
        this.userName = userName;
        this.password = password;
        byteImages=new ArrayList<>();
    }

    public Usuario(String userName, String password, String nombres, String apellidos, String gender) {
        this.userName = userName;
        this.password = password;
        this.nombres = nombres;
        this.apellidos = apellidos;
        this.gender = gender;
        byteImages=new ArrayList<>();
    }
    
    
    public void actualizarGaleria(Album album, String accion, int ubicacion){
        
        if ("borrar".equals(accion)){
            
            this.galeria.remove(ubicacion);   
        }
        else {
            
            this.galeria.remove(ubicacion); 
            this.galeria.add(ubicacion, album);
        }  
    }
    //Actualia la galeria del usuario al poder borrar un album o actualizar su informacion

    public String getUserName() {
        return userName;
    }

    public String getPassword() {
        return password;
    }

    public ArrayList<Album> getGaleria() {
        return galeria;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setGaleria(ArrayList<Album> galeria) {
        this.galeria = galeria;
    }

    public List<byte[]> getByteImages() {
        return byteImages;
    }
    

    @Override
    public String toString() {
        return "Usuario{" + "userName=" + userName + ", password=" + password + ", nombres=" + nombres + ", apellidos=" + apellidos + ", gender=" + gender + '}';
    }

    @Override
    public int hashCode() {
        int hash = 3;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Usuario other = (Usuario) obj;
        if (!Objects.equals(this.userName, other.userName)) {
            return false;
        }
        if (!Objects.equals(this.password, other.password)) {
            return false;
        }
        if (!Objects.equals(this.nombres, other.nombres)) {
            return false;
        }
        if (!Objects.equals(this.apellidos, other.apellidos)) {
            return false;
        }
        if (!Objects.equals(this.gender, other.gender)) {
            return false;
        }
        return true;
    }
    
    
    
}
