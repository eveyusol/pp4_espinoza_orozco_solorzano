/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Componentes;

import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author josè
 */
public class Album implements Serializable{

    public static ArrayList<Album> loadAlbums() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    private String nombre;
    private String descripcion;
    private ArrayList<Foto> fotos;

    public Album() {
        
    }

    public Album(String nombre, String descripcion) {
        this.nombre = nombre;
        this.descripcion = descripcion;
    }

    
    public Album(String nombre, String descripcion, ArrayList<Foto> fotos) {
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.fotos = fotos;
    }
    
    public void actualizarAlbum(Foto foto, String accion, int ubicacion){
        
        if ("borrar".equals(accion)){
            
            this.fotos.remove(ubicacion);   
        }
        else {
            
            this.fotos.remove(ubicacion); 
            this.fotos.add(ubicacion, foto);
        }   
    }
    
    //metodo que se encarga de actualiar un album al borrar una foto o almacenar la actualizacion de dicha foto

    public String getNombre() {
        return nombre;
    }

    public ArrayList<Foto> getFotos() {
        return fotos;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public void setFotos(ArrayList<Foto> fotos) {
        this.fotos = fotos;
    }
    
    
    
}
